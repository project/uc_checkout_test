What is UC Checkout Test?
==========================
UC Checkout Test is a development module for the express purpose of rapidly testing the checkout
process. This module will automatically pre-fill all mandatory checkout fields, will automatically
reset all stock levels, and remove the order upon completion. This allows for easier access to 
testing specific portions of the checkout, as well as theming pages like the checkout review and 
confirmation pages.


UC Checkout Test Settings
==========================
The available settings are as follows
  * Enable or disable stock quantity reset
  * Enable or disable order reset
  * Limit checkout pre-fill to specific roles
  * Manually set the test CC and email address used

The settings page is located under the store menu at /admin/store/checkout-test


Sponsors
=========
This project is currently sponsored by Ciplex. Ciplex is a full-service interactive and marketing
agency that creates award winning design and development solutions, and is located in Los Angeles,
California. http://ciplex.com