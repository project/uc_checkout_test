(function($) {
  Drupal.behaviors.ucCheckoutTester = {
	  attach: function (context) {
      if(Drupal.settings.uc_checkout_test.enabled){
        /* Email Information */
        $('.page-cart #edit-panes-customer-primary-email').attr('value', Drupal.settings.uc_checkout_test.email);
        
        /* Delivery Information */
        $('.page-cart #edit-panes-delivery-delivery-first-name').attr('value', 'John');
        $('.page-cart #edit-panes-delivery-delivery-last-name').attr('value', 'Doe');
        $('.page-cart #edit-panes-delivery-delivery-company').attr('value', 'Apple');
        $('.page-cart #edit-panes-delivery-delivery-street1').attr('value', '1 Infinite Loop');
        $('.page-cart #edit-panes-delivery-delivery-street2').attr('value', '');
        $('.page-cart #edit-panes-delivery-delivery-city').attr('value', 'Cupertino');
        $('.page-cart #edit-panes-delivery-delivery-zone').attr('value', 12);
        $('.page-cart #edit-panes-delivery-delivery-postal-code').attr('value', '95014');
        $('.page-cart #edit-panes-delivery-delivery-phone').attr('value', '123-1234-1234');
        
        /* Billing Information */
        $('.page-cart #edit-panes-billing-billing-first-name').attr('value', 'John');
        $('.page-cart #edit-panes-billing-billing-last-name').attr('value', 'Doe');
        $('.page-cart #edit-panes-billing-billing-company').attr('value', 'Apple');
        $('.page-cart #edit-panes-billing-billing-street1').attr('value', '1 Infinite Loop');
        $('.page-cart #edit-panes-billing-billing-street2').attr('value', '');
        $('.page-cart #edit-panes-billing-billing-city').attr('value', 'Cupertino');
        $('.page-cart #edit-panes-billing-billing-zone').attr('value', 12);
        $('.page-cart #edit-panes-billing-billing-postal-code').attr('value', '95014');
        $('.page-cart #edit-panes-billing-billing-phone').attr('value', '123-1234-1234');
        
        /* CC Information */
        $('.page-cart #edit-panes-payment-details-cc-number').attr('value', Drupal.settings.uc_checkout_test.cc);
        $('.page-cart #edit-panes-payment-details-cc-exp-year').attr('value', '2020');
        $('.page-cart #edit-panes-payment-details-cc-cvv').attr('value', '111');
      }
    }
  };
})(jQuery);